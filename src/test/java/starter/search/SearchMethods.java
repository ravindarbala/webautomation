package starter.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchMethods extends UIInteractionSteps {

  @Step("Search for listing")
  public void performSearch() {
    $(SearchPage.BROWSE_DROPDOWN).click();
    $(SearchPage.REAL_ESTATE_LINK).click();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    $(SearchPage.SEARCH_RESIDENTIAL).click();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public boolean isListofOptionShowed() {
    return $(SearchPage.LISTING_ITEM).isDisplayed();
  }
  public Map<String,String> clickToSeeDetails(){
    $(SearchPage.LISTING_ITEM).click();
    List<WebElement> tr = $(SearchPage.DETAILS).findElements(By.tagName("tr"));
    Map<String,String> values = new HashMap();
    for(WebElement element:tr){
      element.findElement(By.tagName("th")).getText();
      values.put(element.findElement(By.tagName("th")).getText(),element.findElement(By.tagName("td")).getText());
    }
    return values;


  }


}
