package starter.search;

import org.openqa.selenium.By;

public class SearchPage {

  static By SEARCH_FIELD = By.id("searchString");
  static By SEARCH_BUTTON = By.cssSelector("button[value='Search']");
  static By SEARCH_FIELD_ON_RESULT_PAGE = By.cssSelector("#search_form_input");
  static By SEARCH_BUTTON_ON_RESULT_PAGE = By.cssSelector("#search_button");
  static By BROWSE_DROPDOWN = By.cssSelector("a#SiteHeader_SiteTabs_browseDropDownLink");
  static By REAL_ESTATE_LINK =By.cssSelector("a#SiteHeader_SiteTabs_BrowseDropDown_realEstateLink");
  static By SEARCH_RESIDENTIAL =By.cssSelector("button#forSaleButton");
  static By LISTING_ITEM = By.cssSelector("div#tns1-item0 > div");
  static By DETAILS = By.cssSelector("table#ListingAttributes");

}
