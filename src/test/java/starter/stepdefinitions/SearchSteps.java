package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import java.util.Map;
import net.thucydides.core.annotations.Steps;
import starter.navigation.NavigateTo;
import starter.search.SearchMethods;
import static org.assertj.core.api.Assertions.assertThat;

public class SearchSteps {

  @Steps
  NavigateTo navigateTo;

  @Steps
  SearchMethods searchMethods;

  @Given("^the user navigated to trade url$")
  public void the_user_navigated_to_trade_me() {
    navigateTo.navigateToUrl();
  }


  @And("the user search for realestate")
  public void theUserEnteredSearchTextAs() {
    searchMethods.performSearch();

  }

  @Then("the user able to see the list of available options")
  public void theUserAbleToSeeTheListOfAvailableOptions() {
    searchMethods.isListofOptionShowed();


  }

  @Then("the user click on first property to see the details")
  public void theUserClickOnFirstPropertyToSeeTheDetails() {
    Map<String, String> propertyKeyValues = searchMethods.clickToSeeDetails();
    assertThat(!propertyKeyValues.isEmpty()).isTrue();



  }


}
