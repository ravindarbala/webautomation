package starter.navigation;

import net.thucydides.core.annotations.Step;

public class NavigateTo {

    HomePage homePage;

    @Step("Open the trademe home page")
    public void navigateToUrl() {
        homePage.open();
    }
}
